<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Article::class, inversedBy="article")
     */
    private $article_categorie;

    public function __construct()
    {
        $this->article_categorie = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticleCategorie(): Collection
    {
        return $this->article_categorie;
    }

    public function addArticleCategorie(Article $articleCategorie): self
    {
        if (!$this->article_categorie->contains($articleCategorie)) {
            $this->article_categorie[] = $articleCategorie;
        }

        return $this;
    }

    public function removeArticleCategorie(Article $articleCategorie): self
    {
        $this->article_categorie->removeElement($articleCategorie);

        return $this;
    }
}
